# Generated by Django 4.2.7 on 2023-12-02 23:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0015_remove_player_character_remove_player_level_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Character',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('level', models.SmallIntegerField()),
            ],
        ),
    ]
