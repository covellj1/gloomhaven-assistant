from django.contrib import admin
from .models import Campaign, Player, Character

# Register your models here.
@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    list_display = ["party_name"]

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ["name", "campaign"]

@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display = ["name",  "Class", "level", "player"]
