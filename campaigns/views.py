from django.shortcuts import render, redirect, get_object_or_404
from .models import Campaign, Player
from .forms import CampaignForm, PlayerForm, CharacterForm

# Create your views here.
def campaign_list(request):
    campaign_list = Campaign.objects.all()
    context = {"campaign_list": campaign_list}
    return render(request, "campaigns/campaign_list.html", context)

def create_players(request, id):
    campaign = Campaign.objects.get(id=id)
    player_num = campaign.players.count() + 1
    if request.method == 'POST':
        player_form = PlayerForm(request.POST, prefix='player')
        character = CharacterForm(request.POST, prefix='character')
        if player_form.is_valid() and character.is_valid:
            player_form = player_form.save(False)
            player_form.campaign = campaign

            character = character.save(False)
            player_form.character = character
            player_form = player_form.save()
            character.save()
            return redirect('create_players', id=id)
    else:
        player_form = PlayerForm(prefix='player')
        character_form = CharacterForm(prefix='character')
        players = campaign.players.all()
        context = {'campaign':campaign, 'player_form':player_form, 'character_form': character_form, 'player_num': player_num, 'players':players}
        return render(request, 'campaigns/create_players.html', context)


def create_campaign(request):
    if request.method == "POST":
        form = CampaignForm(request.POST)
        if form.is_valid():
            campaign = form.save()
            return redirect("create_players", id=campaign.id)
    else:
        form = CampaignForm()
    context = {'form': form}
    return render(request, 'campaigns/create_campaign.html', context)

def delete_campaign(request, id):
    campaign = Campaign.objects.get(id=id)
    if request.method == 'POST':
        campaign.delete()
        return redirect('campaign_list')
    else:
        context = {'campaign': campaign}
        return render(request, 'campaigns/delete_campaign.html', context)
