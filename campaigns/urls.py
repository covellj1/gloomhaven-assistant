from django.urls import path
from campaigns.views import campaign_list, create_players, create_campaign, delete_campaign


urlpatterns = [
    path('', campaign_list, name='campaign_list'),
    path('<int:id>/players', create_players, name='create_players'),
    path('create/', create_campaign, name='create_campaign'),
    path('<int:id>/delete/', delete_campaign, name='delete_campaign')

]
