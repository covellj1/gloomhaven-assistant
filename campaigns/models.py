from django.db import models

# Create your models here.

class Player(models.Model):
    name = models.CharField(max_length=50)
    campaign = models.ForeignKey("Campaign", related_name="players", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Campaign(models.Model):
    party_name = models.CharField(max_length=200)


    def __str__(self):
        return self.party_name

class Character(models.Model):
    name = models.CharField(max_length=100, null=True)
    CLASSES = [
        ('Brute','Brute'),
        ('Scoundrel', 'Scoundrel'),
        ('Hatchet', 'Hatchet'),
        ('Red Guard', 'Red Guard')
    ]
    Class = models.CharField(max_length=50, choices=CLASSES)
    LEVELS = [
        (1,1),
        (2,2),
        (3,3),
        (4,4),
        (5,5),
        (6,6),
        (7,7),
        (8,8),
        (9,9)
    ]
    level = models.SmallIntegerField(choices = LEVELS, default=1)

    player = models.OneToOneField("Player", related_name="character", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.Class
