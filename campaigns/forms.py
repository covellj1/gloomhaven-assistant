from django.forms import ModelForm
from .models import Campaign, Player, Character

class CampaignForm(ModelForm):
    class Meta:
        model = Campaign
        fields = ["party_name"]

class PlayerForm(ModelForm):
    class Meta:
        model = Player
        fields = ["name"]

class CharacterForm(ModelForm):
    class Meta:
        model = Character
        fields = ["name", "Class", "level"]
